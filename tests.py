"""This module create for testing rm utility"""
import unittest
import shutil
import datetime
import os
import rm.delete

DEFAULT_TRASH_ADRESS = os.path.expanduser("~/rm_test/test_trash")
DEFAULT_INFO_ADRESS = os.path.expanduser("~/rm_test/Output.txt")
DEFAULT_TRASH_SIZE = 1000000


class TestRMPY(unittest.TestCase):


    def setUp(self):
        os.makedirs(DEFAULT_TRASH_ADRESS)
        os.makedirs("test_dir/dir")
        with open(DEFAULT_INFO_ADRESS, "w"):
            pass
        files = ["a", "b", "c"]
        for file in files:
            with open("test_dir/%s.txt" % file, "w"):
                pass


    def tearDown(self):
        shutil.rmtree("test_dir")
        shutil.rmtree(os.path.expanduser("~/rm_test"))


    def test_simple_remove(self):
        file = "test_dir/a.txt"
        rm.delete.remove(file, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "a.txt")))
        self.assertFalse(os.path.exists(file))


    def test_simple_restore(self):
        file = "test_dir/a.txt"
        rm.delete.remove(file, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        rm.delete.restore("a.txt", DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)
        self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "a.txt")))
        self.assertTrue(os.path.exists("test_dir/a.txt"))


    def test_remove_dir(self):
        dir = "test_dir/dir"
        rm.delete.remove(dir, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        self.assertFalse(os.path.exists(dir))
        self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "dir")))


    def test_restore_dir(self):
        dir = "test_dir/dir"
        rm.delete.remove(dir, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        rm.delete.restore("dir", DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)
        self.assertTrue(os.path.exists("test_dir/dir"))
        self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "dir")))


    def test_restore_with_replace(self):
        replace = True
        dir = "test_dir/dir"
        with open("test_dir/dir/1", "w"):
            pass
        rm.delete.remove(dir, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        os.makedirs("test_dir/dir")
        rm.delete.restore("dir", DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS, replace)
        self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "dir")))
        self.assertTrue(os.path.exists("test_dir/dir/1"))


    def test_files_to_remove_and_restore(self):
        files_in_test = os.listdir("test_dir")
        for file in files_in_test:
            rm.delete.remove(os.path.join("test_dir", file), DEFAULT_TRASH_ADRESS,
                          DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
            self.assertFalse(os.path.exists(os.path.join("test_dir", file)))
            self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, file)))

        files_in_test = os.listdir(DEFAULT_TRASH_ADRESS)

        for file in files_in_test:
            rm.delete.restore(file, DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)
            self.assertTrue(os.path.exists(os.path.join("test_dir", file)))
            self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, file)))


    def test_remove_and_restore_in_others_dir(self):
        file1 = "test_dir/a.txt"
        shutil.move("test_dir/b.txt", os.path.expanduser("~/rm_test/b.txt"))
        file2 = os.path.expanduser("~/rm_test/b.txt")
        shutil.move("test_dir/c.txt", os.path.expanduser("~/c.txt"))
        file3 = os.path.expanduser("~/c.txt")
        rm.delete.remove(file1, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        rm.delete.remove(file2, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        rm.delete.remove(file3, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)

        self.assertFalse(os.path.exists(file1))
        self.assertFalse(os.path.exists(file2))
        self.assertFalse(os.path.exists(file3))
        self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, os.path.basename(file1))))
        self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, os.path.basename(file2))))
        self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, os.path.basename(file3))))

        rm.delete.restore(os.path.basename(file1), DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)
        rm.delete.restore(os.path.basename(file2), DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)
        rm.delete.restore(os.path.basename(file3), DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)

        self.assertTrue(os.path.exists(file1))
        self.assertTrue(os.path.exists(file2))
        self.assertTrue(os.path.exists(file3))
        self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS,
                                                     os.path.basename(file1))))
        self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS,
                                                     os.path.basename(file2))))
        self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS,
                                                     os.path.basename(file3))))
        shutil.move(file2, "test_dir/b.txt")
        shutil.move(file3, "test_dir/c.txt")


    def test_remove_by_regular(self):
        count = 0
        files_in_test = []
        while count != 3:
            filename = "/".join(["test_dir", "a" + str(count)])
            with open(filename, "w"):
                pass
            files_in_test.append(filename)
            count += 1
        rm.delete.remove_regular("test_dir", "a", DEFAULT_TRASH_ADRESS,
                              DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        files_in_trash = os.listdir(DEFAULT_TRASH_ADRESS)
        for file in files_in_trash:
            self.assertFalse(os.path.exists(os.path.join("test_dir", file)))
            self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, file)))


    def test_dry_run_remove(self):
        dry = True
        rm.delete.remove("test_dir/a.txt", DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE,
                      DEFAULT_INFO_ADRESS, dry)
        self.assertTrue(os.path.exists("test_dir/a.txt"))
        self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "a.txt")))


    def test_dry_run_restore(self):
        dry = True
        replace = False
        rm.delete.remove("test_dir/a.txt", DEFAULT_TRASH_ADRESS,
                      DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        rm.delete.restore("a.txt", DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS, replace, dry)
        self.assertFalse(os.path.exists("test_dir/a.txt"))
        self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "a.txt")))


    def test_trash_size(self):
        dir = "test_dir/dir"
        new_trash_size = 100
        rm.delete.remove(dir, DEFAULT_TRASH_ADRESS, new_trash_size, DEFAULT_INFO_ADRESS)
        self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "dir")))
        self.assertTrue(os.path.exists(dir))


    def test_conflict_in_trash(self):
        file1 = "test_dir/a.txt"
        rm.delete.remove(file1, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        with open("test_dir/a.txt", "w"):
            pass
        file2 = "test_dir/a.txt"
        rm.delete.remove(file2, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "a.txt")))
        self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "a.txt0")))


    def test_conflict_in_restore(self):
        file1 = "test_dir/a.txt"
        rm.delete.remove(file1, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        with open("test_dir/a.txt", "w"):
            pass
        rm.delete.restore(file1, DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)
        self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "a.txt")))
        self.assertTrue(os.path.exists("test_dir/a.txt"))


    def test_remove_file_remove_its_dir(self):
        file = "test_dir/a.txt"
        rm.delete.remove(file, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        rm.delete.remove("test_dir", DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        rm.delete.restore("a.txt", DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)
        self.assertTrue(os.path.exists("test_dir/a.txt"))


    def test_remove_and_restore_1000_file(self):
        count = 0
        files_in_test = []
        while count != 1000:
            filename = "/".join(["test_dir", str(count)])
            with open(filename, "w"):
                pass
            files_in_test.append(filename)
            rm.delete.remove(filename, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
            count += 1
        files_in_trash = os.listdir(DEFAULT_TRASH_ADRESS)
        for file in files_in_trash:
            self.assertFalse(os.path.exists(os.path.join("test_dir", file)))
            self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, file)))
        files_in_trash = os.listdir(DEFAULT_TRASH_ADRESS)
        for file in files_in_trash:
            rm.delete.restore(file, DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)
        files_in_trash = os.listdir(DEFAULT_TRASH_ADRESS)
        for file in files_in_trash:
            self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, file)))
            self.assertTrue(os.path.exists(os.path.join("test_dir", file)))


    def test_clear_trash(self):
        count = 0
        files_in_test = []
        while count != 100:
            filename = "/".join(["test_dir", str(count)])
            with open(filename, "w"):
                pass
            files_in_test.append(filename)
            rm.delete.remove(filename, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
            count += 1
        files_in_trash = os.listdir(DEFAULT_TRASH_ADRESS)
        rm.delete.clear_trash(DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)
        for file in files_in_trash:
            self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, file)))


    def test_output_trash(self):
        file = "test_dir/a.txt"
        rm.delete.remove(file, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        dir = "test_dir/dir"
        rm.delete.remove(dir, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        dict = rm.delete.get_trash_files(DEFAULT_TRASH_ADRESS)
        self.assertEqual(0, dict["a.txt"])
        self.assertEqual(4096, dict["dir"])


    def test_size_policy(self):
        file = "test_dir/a.txt"
        rm.delete.remove(file, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        dir = "test_dir/dir"
        rm.delete.remove(dir, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        rm.delete.clear_trash_by_size(4000, DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)
        self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "a.txt")))
        self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "dir")))


    def test_day_policy(self):
        file = "test_dir/a.txt"
        rm.delete.remove(file, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        rm.delete.clear_trash_by_day(datetime.datetime.today().isoweekday(),
                                  DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)
        self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "a.txt")))
        dir1 = "test_dir/dir"
        rm.delete.remove(dir1, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        rm.delete.clear_trash_by_day(datetime.datetime.today().isoweekday()+1,
                                  DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)
        self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "dir")))


    def test_manual_policy(self):
        file1 = "test_dir/a.txt"
        rm.delete.remove(file1, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        file2 = "test_dir/b.txt"
        rm.delete.remove(file2, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        dir = "test_dir/dir"
        rm.delete.remove(dir, DEFAULT_TRASH_ADRESS, DEFAULT_TRASH_SIZE, DEFAULT_INFO_ADRESS)
        to_clear = [os.path.basename(file1), os.path.basename(dir)]
        rm.delete.clear_trash_manual(to_clear, DEFAULT_TRASH_ADRESS, DEFAULT_INFO_ADRESS)
        self.assertTrue(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "b.txt")))
        self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "a.txt")))
        self.assertFalse(os.path.exists(os.path.join(DEFAULT_TRASH_ADRESS, "dir")))


if __name__ == '__main__':
    unittest.main()
