"""Utility to delete files / directories

    Includes modules: 
        app.py - Processing parameters entered in the terminal. The logic of the program
        delete.py - The main functions of the utility
        config_tools.py - Processing the configuration file
"""
