#!/usr/bin/python
# -*- coding: utf-8 -*-
"""The main functions of the deleting utility
 *constants aviable:
    - TRASH_ADRESS - default adress of trash

    - TRASH_SIZE - default size of trash

    - INFO_ADRESS - default path to info

 *functions aviable:
    - get_file_size(arg) - takes adress(arg), returns
    the size of the file contents

    - clear_trash(trash_adress=TRASH_ADRESS, info_adress=INFO_ADRESS) -
    cleaning the trash

    - clear_trash_manual(files_to_delete, trash_adress=TRASH_ADRESS,
                         info_adress=INFO_ADRESS) - removes the specified
    list of files from the trash

    - clear_trash_by_day(day=0, trash_adress=TRASH_ADRESS,
                         info_adress=INFO_ADRESS) - сleaning policy by time

    - clear_trash_by_size(size=5000, trash_adress=TRASH_ADRESS,
                          info_adress=INFO_ADRESS) - сleaning policy by size

    - delete_file(file_to_delete) - absolute file/dir deletion

    - get_trash_files(trash_adress=TRASH_ADRESS) - return dictionary with
    information about files in trash

    - rename(arg, files_in_trash, dry=False) - takes filename
     and list of files. If such file is in the list, the original file is renamed

    - remove(arg, trash_adress=TRASH_ADRESS, trash_size=TRASH_SIZE,
             info_adress=INFO_ADRESS, dry=False) - takes file path.
    Returns new path and two booleans parametres. They can be used to
    determine where the error occurred

    - remove_regular(arg, regular_value, trash_adress=TRASH_ADRESS, trash_size=TRASH_SIZE,
                   info_adress=INFO_ADRESS, dry=False) - takes directory and
    regular expression. Return dictionary with file path and success of deletion

    - search_file_info(info_adress, new_file_name, dry) - looking for information about 
    a file by its name

    - restore(new_file_name, trash_adress=TRASH_ADRESS,
            info_adress=INFO_ADRESS, replace=False, dry=False) - restores the file,
    using the information from info_adress
"""

import shutil
import os
import datetime
from enum import Enum


DEFAULT_TRASH_ADRESS = os.path.expanduser("~/rm/trash")
DEFAULT_TRASH_SIZE = 100000
DEFAULT_INFO_ADRESS = os.path.expanduser("~/rm/Output.txt")


class Codes(Enum):
    """values returned by functions"""
    GOOD = 0
    CONFLICT = 1
    BAD = 2
    NO_FILE = 3
    NO_SPACE = 4


def get_file_size(path):
    size = 0
    if os.path.isdir(path):
        for file_in_dir in os.listdir(path):
            file_for_size = os.path.join(path, file_in_dir)
            if os.path.isdir(file_for_size):
                size += os.path.getsize(file_for_size) + get_file_size(file_for_size)
            else:
                size += os.path.getsize(file_for_size)
    else:
        size += os.path.getsize(path)
    return size


def clear_trash(trash_adress=DEFAULT_TRASH_ADRESS, info_adress=DEFAULT_INFO_ADRESS):
    """Completely clears the trash"""
    deleted_files = []
    files_in_trash = os.listdir(trash_adress)
    with open(info_adress, "w"):
        pass
    for file_in_trash in files_in_trash:
        file_to_delete = os.path.join(trash_adress, file_in_trash)
        deleted_files.append(file_to_delete)
        delete_file(file_to_delete)
    return deleted_files


def clear_trash_manual(files_to_delete, trash_adress=DEFAULT_TRASH_ADRESS,
                       info_adress=DEFAULT_INFO_ADRESS):
    """Removes the specified files from the trash"""
    deleted_files = []
    for file_in_trash in files_to_delete:
        file_to_delete = os.path.join(trash_adress, file_in_trash)
        with open(info_adress, "r") as info:
            lines = [line.strip() for line in info]
        with open(info_adress, "w") as record:
            for line in lines:
                values = line.split(" ")
                if values[0] != os.path.basename(file_to_delete):
                    record.write(line+"\n")
        deleted_files.append(file_to_delete)
        delete_file(file_to_delete)
    return deleted_files


def clear_trash_by_day(day=0, trash_adress=DEFAULT_TRASH_ADRESS, info_adress=DEFAULT_INFO_ADRESS):
    """Cleaning policy by time"""
    deleted_files = []
    files_in_trash = os.listdir(trash_adress)
    for file_in_trash in files_in_trash:
        if datetime.datetime.today().isoweekday() == day:
            file_to_delete = os.path.join(trash_adress, file_in_trash)
            with open(info_adress, "r") as info:
                lines = [line.strip() for line in info]
            with open(info_adress, "w") as record:
                for line in lines:
                    values = line.split(" ")
                    if values[0] != os.path.basename(file_to_delete):
                        record.write(line+"\n")
            deleted_files.append(file_to_delete)
            delete_file(file_to_delete)
    return deleted_files


def clear_trash_by_size(size=5000, trash_adress=DEFAULT_TRASH_ADRESS,
                        info_adress=DEFAULT_INFO_ADRESS):
    """Cleaning policy by size"""
    deleted_files = []
    file_size = 0
    files_in_trash = os.listdir(trash_adress)
    for file_in_trash in files_in_trash:
        file_to_delete = os.path.join(trash_adress, file_in_trash)
        if os.path.isdir(file_to_delete):
            file_size = os.path.getsize(file_to_delete)
        file_size += get_file_size(file_to_delete)
        if file_size > size:
            with open(info_adress, "r") as info:
                lines = [line.strip() for line in info]
            with open(info_adress, "w") as record:
                for line in lines:
                    values = line.split(" ")
                    if values[0] != os.path.basename(file_to_delete):
                        record.write(line+"\n")
            deleted_files.append(file_to_delete)
            delete_file(file_to_delete)
    return deleted_files


def delete_file(file_to_delete):
    """Absolute file/dir deletion"""
    if os.path.isfile(file_to_delete):
        os.remove(file_to_delete)
    if os.path.isdir(file_to_delete):
        shutil.rmtree(file_to_delete)


def get_trash_files(trash_adress=DEFAULT_TRASH_ADRESS):
    """Takes trash adress. Return the dictionary.
    Filename is a key. Value - the size of this file"""
    file_size = 0
    files_in_trash = {}
    try:
        for file_in_trash in os.listdir(trash_adress):
            file_path = os.path.join(trash_adress, file_in_trash)
            if os.path.isdir(file_path):
                file_size = os.path.getsize(file_path)
            file_size += get_file_size(file_path)
            files_in_trash[file_in_trash] = file_size
        return files_in_trash
    except:
        return files_in_trash


def rename(file_adress, files_in_trash, dry=False):
    "Rename file if the trash already has one"
    count = 0
    good_name = False
    while not good_name:
        for file_in_trash in files_in_trash:
            if "".join([os.path.basename(file_adress), str(count)]) == os.path.basename(file_in_trash):
                count = count + 1
                good_name = False
                break
            good_name = True

    new_file_adress = "".join([file_adress, str(count)])
    if not dry:
        os.rename(file_adress, new_file_adress)
    file_adress = new_file_adress
    return file_adress


def remove(file_to_delete, trash_adress=DEFAULT_TRASH_ADRESS, trash_size=DEFAULT_TRASH_SIZE,
           info_adress=DEFAULT_INFO_ADRESS, dry=False):
    """Delete function with file falidation.
    Information about the current file address is saved in info_adress
    """
    free_trash_size = 0
    if os.path.exists(file_to_delete):
        file_path = os.path.join(os.getcwd(), file_to_delete)
        files_in_trash = os.listdir(trash_adress)
        file_size = get_file_size(file_to_delete) + os.path.getsize(file_to_delete)
        if (file_size + get_file_size(trash_adress)) <= trash_size:
            for file_in_trash in files_in_trash:
                if os.path.basename(file_to_delete) == os.path.basename(file_in_trash):
                    file_to_delete = rename(file_to_delete, files_in_trash, dry)
            if not dry:
                with open(info_adress, "a") as record:
                    file_info = " ".join([os.path.basename(file_to_delete), file_path, "\n"])
                    record.write(file_info)
                try:
                    shutil.move(file_to_delete, trash_adress)
                    free_trash_size = trash_size - file_size - get_file_size(trash_adress)
                except:
                    return Codes.BAD.value, file_path, trash_size
            file_path = os.path.join(trash_adress, os.path.basename(file_to_delete))
            return  Codes.GOOD.value, file_path, free_trash_size
        else:
            return Codes.NO_SPACE.value, file_path, free_trash_size
    else:
        return Codes.NO_FILE.value, file_to_delete, free_trash_size


def remove_regular(dir_adress, regular_value, trash_adress=DEFAULT_TRASH_ADRESS,
                   trash_size=DEFAULT_TRASH_SIZE, info_adress=DEFAULT_INFO_ADRESS,
                   dry=False):
    """Function for remove files by regular expression. Return lists: deleted files,
    undeleted files, renamed files.
    """
    deleted_files = []
    undeleted_files = []
    renamed_files = {}
    try:
        files_in_dir = os.listdir(dir_adress)
        for file_in_dir in files_in_dir:
            if regular_value in os.path.basename(file_in_dir):
                target_file = os.path.join(dir_adress, file_in_dir)
                code, new_file_adress, _ = remove(target_file, trash_adress, trash_size,
                                               info_adress, dry)
                if os.path.basename(target_file) != os.path.basename(new_file_adress):
                    renamed_files[os.path.basename(target_file)] = os.path.basename(new_file_adress)
                if code == Codes.GOOD.value:
                    deleted_files.append(target_file)
                else:
                    undeleted_files.append(target_file)
        return Codes.GOOD.value, deleted_files, undeleted_files, renamed_files
    except:
        return Codes.BAD.value, deleted_files, undeleted_files, renamed_files


def search_file_info(info_adress, new_file_name, dry):
    adress_file = ""
    with open(info_adress, "r") as info:
        lines = [line.strip() for line in info]
    with open(info_adress, "w") as record:
        for line in lines:
            values = line.split(" ")
            if values[0] != os.path.basename(new_file_name):
                record.write(line+"\n")
            else:
                new_file_name = values[0]
                adress_file = values[1]
                if dry:
                    record.write(line+"\n")
    return adress_file


def restore(new_file_name, trash_adress=DEFAULT_TRASH_ADRESS,
            info_adress=DEFAULT_INFO_ADRESS, replace=False, dry=False):
    """Restore function with file falidation. Returns success of restoring and adress"""
    file_in_trash = os.path.join(trash_adress, new_file_name)
    adress_file = ""

    if os.path.exists(file_in_trash):
        adress_file = search_file_info(info_adress, new_file_name, dry)
        if os.path.exists(adress_file):
            if replace:
                if not dry:
                    delete_file(adress_file)
                    os.rename(file_in_trash, adress_file)
                return Codes.GOOD.value, adress_file
            else:
                with open(info_adress, "a") as record:
                    file_info = " ".join([os.path.basename(new_file_name), adress_file, "\n"])
                    record.write(file_info)
                return Codes.CONFLICT.value, adress_file
        else:
            try:
                if not dry:
                    if not os.path.exists(os.path.dirname(adress_file)):
                        os.makedirs(os.path.dirname(adress_file))
                    os.rename(file_in_trash, adress_file)
            except OSError:
                return Codes.BAD.value, adress_file

            return Codes.GOOD.value, adress_file

    else:
        return Codes.NO_FILE.value, adress_file
