#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Main module of deleting utility
 *constants aviable:
    - LOG_FORMAT - format of logging

    - LOGGER - object of log

    - TERMINAL_MESSAGE - log for terminal

 *functions aviable:
    - setup_log(adress_log, log_level, silent) - takes adress to which the log
    will be record.

    - check_sys_dir(paths) - takes adresses(paths).
    Check the system directory

    - show_trash_files(trash_adress, limit=10) - takes adress of trash. Prints every

    - print_progress(iteration, total) - shows the progress of execution
    limit line to the terminal

    - write_remove_message(code, arg, new_adress, verbose=False) - writes a message about
    deleting a file based on the return code of the removing function

    - write_regular_remove_message(code, arg, deleted_files, undeleted_files,
                             renamed_files, verbose=False) - writes a message about
    regular deleting files based on the return code of the regular removing function

    - write_restore_message(code, arg, adress_file, verbose=False) - writes a message about
    restoring a file based on the return code of the restoring function
"""

import sys
import argparse
import os
import logging
import rm.delete
from rm.config_tools import load_config


LOG_FORMAT = "Time: %(asctime)s  Type: [%(levelname)s] Message: %(message)s"
LOGGER = logging.getLogger(__name__)
TERMINAL_MESSAGE = logging.StreamHandler()


def setup_log(adress_log, log_level="DEBUG", silent=False):
    """Function for redefining the log"""
    if not silent:
        LOGGER.addHandler(TERMINAL_MESSAGE)
    try:
        LOGGER.setLevel(log_level)
        logging.basicConfig(filename=adress_log, format=LOG_FORMAT)
        return True
    except:
        return False


def print_progress(iteration, total):
    """display the progress of the program in percent"""
    try:
        decimals = 1
        progress = 100 * (iteration / float(total))
        percent = ("{0:." + str(decimals) + "f}").format(progress)
        print("|%s%%|" % percent)
    except ZeroDivisionError:
        pass


def check_sys_dir(paths):
    """Check for the number of files. Checking for system folders"""
    for path in paths:
        arg_adress = os.path.abspath(path).split("/")
        if len(arg_adress) == 2 and arg_adress[0] == "":
            message = " ".join(["FATAL!", "This file:", path, "looks like a system file"])
            LOGGER.log(50, message)
            return True
    return False


def show_trash_files(trash_adress, limit=10):
    """Output information of files in trash on terminal"""
    size_of_trash = 0
    count_files = 0
    files_in_trash = rm.delete.get_trash_files(trash_adress)
    for file_in_trash in files_in_trash:
        size_of_file = files_in_trash[file_in_trash]
        size_of_trash += size_of_file
        message = " ".join([file_in_trash, "|Size:", str(size_of_file)])
        print message
        count_files = count_files + 1
        if count_files == int(limit):
            raw_input()
            count_files = 0
    message = " ".join(["Size of trash", str(size_of_trash)])
    print message
    print "___________________________"


def write_remove_message(code, arg, new_adress, verbose=False):
    message = ""
    if code == rm.delete.Codes.BAD.value:
        message = " ".join(["Unknown error"])
        LOGGER.log(40, message)
    elif code == rm.delete.Codes.GOOD.value:
        if os.path.basename(arg) != os.path.basename(new_adress):
            message = " ".join(["File", os.path.basename(arg),
                                "renamed to", os.path.basename(new_adress)])
            LOGGER.log(20, message)
        message = " ".join(["The file", os.path.basename(arg), "is deleted"])
        if verbose:
            filesize = rm.delete.get_file_size(new_adress) + os.path.getsize(new_adress)
            message = " ".join([message, "| Size of file:", str(filesize)])
        LOGGER.log(10, message)
    elif code == rm.delete.Codes.NO_SPACE.value:
        message = " ".join(["The trash is no place for", os.path.basename(arg)])
        if verbose:
            filesize = rm.delete.get_file_size(arg) + os.path.getsize(arg)
            message = " ".join([message, "| Size of file:", str(filesize),
                                "| Free size of trash:", str(trash_space)])
        LOGGER.log(40, message)
    elif code == rm.delete.Codes.NO_FILE.value:
        message = " ".join(["This file:", os.path.basename(arg), "does not exist"])
        LOGGER.log(40, message)
    return message


def write_regular_remove_message(code, arg, deleted_files, undeleted_files,
                                 renamed_files, verbose=False):
    message = ""
    if code == rm.delete.Codes.GOOD.value:
        for deleted_file in deleted_files:
            deleted_file = os.path.basename(deleted_file)
            for renamed_file in renamed_files:
                if deleted_file == renamed_file:
                    message = " ".join(["File", deleted_file, "renamed to",
                                        renamed_files[renamed_file]])
                    LOGGER.log(20, message)
            message = " ".join(["The file", deleted_file, "is deleted"])
            if verbose:
                filesize = rm.delete.get_file_size(deleted_file) + os.path.getsize(deleted_file)
                message = " ".join([message, "| Size of file:", str(filesize)])
            LOGGER.log(10, message)
        for file_adress in undeleted_files:
            message = " ".join(["The trash is no place for",
                                os.path.basename(file_adress)])
            if verbose:
                filesize = rm.delete.get_file_size(file_adress) + os.path.getsize(file_adress)
                message = " ".join([message, "| Size of file:", str(filesize)])
            LOGGER.log(40, message)
    else:
        message = " ".join(["This directory doesn't exist:", arg])
        LOGGER.log(40, message)
    return message


def write_restore_message(code, arg, adress_file, verbose=False):
    message = ""
    if code == rm.delete.Codes.BAD.value:
        message = " ".join(["Information about this file:",
                            os.path.basename(arg), "has been lost"])
        LOGGER.log(50, message)
    elif code == rm.delete.Codes.GOOD.value:
        message = " ".join(["File", arg, "restore. Adress:", adress_file])
        if verbose:
            filesize = rm.delete.get_file_size(adress_file) + os.path.getsize(adress_file)
            message = " ".join([message, "| Size of file:", str(filesize)])
        LOGGER.log(10, message)
    elif code == rm.delete.Codes.CONFLICT.value:
        message = " ".join(["This file already exist:", os.path.basename(arg)])
        LOGGER.log(30, message)
    elif code == rm.delete.Codes.NO_FILE.value:
        message = " ".join(["Trash does not contain this file:", arg])
        LOGGER.log(40, message)
    return message


def main():
    """The starting function of the block.
    performs interrelations between functions and user
    """
    parser = argparse.ArgumentParser()

    parser.add_argument("-i", help="Ask before deleting", action="store_true")
    parser.add_argument("-r", "--restore", help="Restore file", action="store_true")
    parser.add_argument("-o", "--output", help="Record deleted files in Output.txt")
    parser.add_argument("-R", "--regular", help="Deletion by regular expression")
    parser.add_argument("file", nargs="*", help="Files", type=str)
    parser.add_argument("--config", nargs=2,
                        help="Load config.Enter parametres(config adress, config format[json/txt])")
    parser.add_argument("--dry", action="store_true", help="Simulation of work")
    parser.add_argument("--silent", help="No output", action="store_true")
    parser.add_argument("-v", "--verbose", action="store_true", help="Additional information")
    parser.add_argument("--replace", action="store_true", help="Replaces the file when restored")
    parser.add_argument("--percent", help="No percent", action="store_true")
    parser.add_argument("--show", help="Show trash", nargs="?", const=10)
    parser.add_argument("--tsize", help="Set the size of trash")
    parser.add_argument("--log", help="Log adress")
    parser.add_argument("--level", help="Log level")
    parser.add_argument("--manual", nargs="*", type=str,
                        help="The listed files will be removed from the trash")
    parser.add_argument("--adress", help="Trash adress")
    parser.add_argument("--day", help="Set the delete policy by days")
    parser.add_argument("--size", help="Set the delete policy by size")
    parser.add_argument("--clear", help="Delete files in trash", action="store_true")

    args = parser.parse_args()
    dry = False
    silent = False
    verbose = False
    replace = False
    trash_adress = os.path.expanduser("~/rm/trash")
    info_adress = os.path.expanduser("~/rm/Output.txt")
    trash_size = 1000000
    log_path = os.path.expanduser("~/rm/log.log")
    log_level = "DEBUG"
    size_policy = False
    day_policy = False

    try:
        if args.config:
            config = load_config(args.config[0], args.config[1])
        else:
            config = load_config()
        trash_adress = config["trash"]
        trash_size = int(config["trash_size"])
        info_adress = config["info_path"]
        size = int(config["size"])
        day = int(config["day"])
        dry = bool(config["dry"])
        silent = bool(config["silent"])
        verbose = bool(config["verbose"])
        replace = bool(config["replace"])
        size_policy = bool(config["size_policy"])
        day_policy = bool(config["day_policy"])
        log_path = config["log_path"]
        log_level = config["log_level"]

        """For txt config"""
        if config["dry"] == "False":
            dry = False
        if config["silent"] == "False":
            silent = False
        if config["verbose"] == "False":
            verbose = False
        if config["replace"] == "False":
            replace = False
        if config["size_policy"] == "False":
            size_policy = False
        if config["day_policy"] == "False":
            day_policy = False
    except:
        message = "Error in config. Use default configuration"
        print message
        if not os.path.exists(trash_adress):
            os.makedirs(trash_adress)

    if args.dry:
        dry = True
    if args.silent:
        silent = True
    if args.verbose:
        verbose = True
    if args.replace:
        replace = True

    if args.log:
        log_path = args.log
    if args.level:
        log_level = args.level

    if not os.path.exists(log_path):
        with open(log_path, "w"):
            pass

    if not setup_log(log_path, log_level, silent):
        print "Wrong log information"
        sys.exit()

    if args.adress:
        trash_adress = args.adress
    if args.output:
        info_adress = args.output
    if args.tsize:
        trash_size = int(args.tsize)
    if args.size:
        size = int(args.size)
        size_policy = True
    if args.day:
        day = int(args.day)
        day_policy = True

    if check_sys_dir(args.file):
        sys.exit()
    if len(args.file) > 1000:
        message = "Out of range"
        LOGGER.log(50, message)
        sys.exit()
    total_operations = len(args.file)

    if not os.path.exists(trash_adress):
        message = " ".join(["Trash does not exist. Creating directory", trash_adress])
        LOGGER.log(20, message)
        os.mkdir(trash_adress)
    elif os.path.isfile(trash_adress):
        message = " ".join(["This adress of trash is used by the file", trash_adress])
        LOGGER.log(50, message)
        sys.exit()

    if not os.path.exists(info_adress):
        with open(info_adress, "w"):
            pass
        message = " ".join(["Output.txt does not exist. Creating file", info_adress])
        LOGGER.log(20, message)

    elif os.path.isdir(info_adress):
        message = " ".join(["This adress of information of files is used by the directory",
                            info_adress])
        LOGGER.log(50, message)
        sys.exit()

    if args.file == [] and args.regular:
        args.file = [1]
        args.file[0] = os.getcwd()

    answer = True
    operations = 0

    for arg in args.file:
        if args.i:
            message = " ".join(["Do you want to perform an operation with this file :",
                                arg, "?[y/n]"])
            if raw_input(message) == "y":
                answer = True
            else:
                answer = False

        if answer:

            if args.regular:
                code, deleted_files, undeleted_files, renamed_files = rm.delete.remove_regular(arg, args.regular,
                                                                                               trash_adress, trash_size,
                                                                                               info_adress, dry)
                write_regular_remove_message(code, arg, deleted_files, undeleted_files,
                                             renamed_files, verbose)

            if args.restore:
                code, adress_file = rm.delete.restore(arg, trash_adress, info_adress, replace, dry)
                write_restore_message(code, arg, adress_file, verbose)

            if args.file and not args.regular and not args.restore:
                code, new_adress = rm.delete.remove(arg, trash_adress, trash_size,
                                                    info_adress, dry)
                write_remove_message(code, arg, new_adress, verbose)

        operations += 1
        if not silent and args.percent:
            print_progress(operations, total_operations)


    if args.show and not silent:
        show_trash_files(trash_adress, args.show)

    if args.manual:
        deleted_files = rm.delete.clear_trash_manual(args.manual, trash_adress, info_adress)
        for deleted_file in deleted_files:
            message = " ".join(["The file", os.path.basename(deleted_file), "is deleted"])
            LOGGER.log(10, message)
    if size_policy:
        deleted_files = rm.delete.clear_trash_by_size(size, trash_adress, info_adress)
        for deleted_file in deleted_files:
            message = " ".join(["The file", os.path.basename(deleted_file), "is deleted"])
            LOGGER.log(10, message)

    if day_policy:
        deleted_files = rm.delete.clear_trash_by_day(day, trash_adress, info_adress)
        for deleted_file in deleted_files:
            message = " ".join(["The file", os.path.basename(deleted_file), "is deleted"])
            LOGGER.log(10, message)

    if args.clear:
        deleted_files = rm.delete.clear_trash(trash_adress, info_adress)
        for deleted_file in deleted_files:
            message = " ".join(["The file", os.path.basename(deleted_file), "is deleted"])
            LOGGER.log(10, message)


if __name__ == '__main__':
    main()
