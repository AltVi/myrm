This is remove utility 
version = 1.2 

Quickstart: 

Change paths in config.json(config.txt) and config_tools.py 
Then write "python setup.py install" in the terminal 

Parametres: 

run in default mode , application removes file or directory: 

    rmpy [path to file] 

To restore the file, use the -r flag: 

    rmpy [path to file] -r 

To remove by regular expression use -R flag: 

    rmpy [path to dir] -R [regular expression] 

If you want the application to ask for permission before any action performed, specidy -i flag: 

    rmpy [path to file] -i

Specifies the path to the file that stores addresses:

    rmpy [path to file] -o

To watch how the application works without any changes being done: 
 
    rmpy [path to fil] --dry 

If you don't want to get any console output, specify --silent flag: 

    rmpy [path to file] --silent 

If you want to get additional Information, specify --verbose flag: 

    rmpy [path to file] --verbose 

If you want to replace file after restoring, specify --replace flag: 

    rmpy [path to file] --replace 

To clear trash by size: 
 
    rmpy [path to file] --size [size] 

To clear trash by time:

    rmpy [path to file] --day [day] 

If you want to load config, write:

    rmpy [path to file] --config [format] [adress] 


Config example: 

JSON
{
  "trash": "/home/droid/python/trash", 
  "log_path": "/home/droid/python/log.log", 
  "info_path": "/home/droid/python/Output.txt", 
  "log_level": "DEBUG", 
  "dry": false, 
  "silent": false, 
  "trash_size": 1000000, 
  "size_policy": false, 
  "size": 10000, 
  "day_policy": false, 
  "day": 6 
}

TXT
trash = /home/droid/python/trash 
log_path = /home/droid/python/log.log 
info_path = /home/droid/python/Output.txt 
log_level = DEBUG 
dry = False 
silent = False 
trash_size = 1000000 
size_policy = False 
size = 10000 
day_policy = False 
day = 6 

params: 
    trash - path to trash 
    log_path - path to log file 
    log_level - set level of logging 
    dry - set dry mode value 
    silent - set silent mode value 
    trash_size - set size of trash 
    size_policy - turns on the policy on size 
    size - set the maximum size of the file for cleaning 
    day_policy - turns on the policy on day 
    day - set the day of the file for cleaning 

Modules description:

1 - app.py Represents application's entry point. The place where all the main functions are called. 

2 - delete.py - Includes functions for removing/restoring files.

3 - load_config.py The module loading configuration files. 
